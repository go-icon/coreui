# coreui
https://coreui.io/icons/

# Installation
`go get -u gitea.com/go-icon/coreui`

# Usage
```go
icon := coreui.BrandGitea()

// Get the raw XML
xml := icon.XML()

// Get something suitable to pass directly to an html/template
html := icon.HTML()
```

# Build
`go generate generate.go`

# New Versions
To update the version of coreui, simply change `coreuiVersion` in `coreui_generate.go` and re-build.

# License
[Library](LICENSE)  
[SVGs](LICENSE.txt)